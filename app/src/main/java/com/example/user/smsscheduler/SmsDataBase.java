package com.example.user.smsscheduler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SmsDataBase extends SQLiteOpenHelper {
    SQLiteDatabase db;
    private static final String db_name = "Sms_DataBases";
    private static final String table_name = "SmsData";
    Cursor res,res1,res2;

    public SmsDataBase(Context context) {
        super(context, db_name, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(" CREATE TABLE " + table_name + " (ID INTEGER PRIMARY KEY AUTOINCREMENT,MBL_NUMBER TEXT,MESSAGE TEXT,STATUS TEXT,DATE_TIME TEXT) ");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(" DROP TABLE IF EXISTS " + table_name);
        onCreate(db);

    }

    public boolean insertData(String number,String txt,String stts,String datetime)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("MBL_NUMBER",number);
        contentValues.put("MESSAGE",txt);
        contentValues.put("STATUS",stts);
        contentValues.put("DATE_TIME",datetime);
        long i = db.insert(table_name,null,contentValues);

        if(i==-1)
            return false;
        else
            return true;

    }
    public Cursor getAlldata()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        res = db.rawQuery("SELECT * FROM " + table_name,null);
      //  Log.i("SmsDataBase",res.toString());
        return res;

    }
    public Cursor getDataByNumber_DateTime(String t1,String t2)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query="SELECT * FROM " + table_name + " WHERE MBL_NUMBER = ? AND DATE_TIME = ?";
        String[] selectionArgs = {t1,t2};

        res1 = db.rawQuery(query,selectionArgs);
        return res1;

    }
    public int update(String s1,String s2)
    {
        Log.i("Update F","S1: "+s1);
        Log.i("Update F","S2: "+s2);
        SQLiteDatabase db = getWritableDatabase();
        ContentValues c = new ContentValues();
        c.put("STATUS","sent");
        int nOr = db.update(table_name,c," MBL_NUMBER = ? AND DATE_TIME = ? ",new String[]{s1,s2});
        return nOr;

    }
    public Cursor getDataByStatus(String stts)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query="SELECT * FROM " + table_name + " WHERE STATUS = ?";
        String[] selectionArgs = {stts};

        res2 = db.rawQuery(query,selectionArgs);
        return res2;

    }

}
