package com.example.user.smsscheduler;

import android.Manifest;
import android.Manifest.permission;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Objects;

public class Sms extends AppCompatActivity implements View.OnClickListener {

    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 1; //permisiion variable for accessing contact list
    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS = 1; //permission for sending data
    private static int FLAG = 0;

    Button contact, setAlarm, cencel;
    Button btnDatePicker, btnTimePicker;
    EditText txtDate, txtTime, phnnmbr, txtmssg;
    String nmbr = null, mssg = "", dateTime = "";
    private int mYear, mMonth, mDay, mHour, mMinute;
    int Year, month, day, hour, Minute;

    SmsDataBase smsDb;  // data base initialyzing
    Calendar cal = Calendar.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sms);
        AlarmManager aM = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        //Initialyzing keyWords
        btnDatePicker = (Button) findViewById(R.id.btn_date);
        btnTimePicker = (Button) findViewById(R.id.btn_time);
        txtDate = (EditText) findViewById(R.id.in_date);
        txtTime = (EditText) findViewById(R.id.in_time);
        phnnmbr = findViewById(R.id.et1);
        txtmssg = findViewById(R.id.et2);
        setAlarm = findViewById(R.id.btn_setalarm);
        contact = findViewById(R.id.btn_contact);
        cencel = findViewById(R.id.btn_cnclAlarm);

        //initialyzing and set on click listener for date and time
        btnDatePicker.setOnClickListener(this);
        btnTimePicker.setOnClickListener(this);


        smsDb = new SmsDataBase(this); //database initialyzing


        if (ContextCompat.checkSelfPermission(this, permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED)  // permission to send message
        {
            ActivityCompat.requestPermissions(this, new String[]{permission.SEND_SMS}, MY_PERMISSIONS_REQUEST_SEND_SMS);
        }

        contact.setOnClickListener(new View.OnClickListener() { //on lcick listener for contact
            @Override
            public void onClick(View v) {
                Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(contactPickerIntent, 1);
            }
        });

        cencel.setVisibility(View.GONE);
        action_setAlarm();
            cencel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cencelAlarm();
                    Toast.makeText(getBaseContext(), "Cenceld...!!", Toast.LENGTH_SHORT).show();
                }
            });


    }

    @Override
    public void onClick(View v) {

        if (v == btnDatePicker) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            Year = year;
                            month = monthOfYear;
                            day = dayOfMonth;


                        }
                    }, mYear, mMonth, mDay);

            datePickerDialog.show();
        }
        if (v == btnTimePicker) {

            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            txtTime.setText(hourOfDay + ":" + minute);
                            hour = hourOfDay;
                            Minute = minute;
                        }
                    }, mHour, mMinute, false);
            timePickerDialog.show();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 1:
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    ContentResolver cr = getContentResolver();


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
                        //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
                    } else {
                        // Android version is lesser than 6.0 or the permission is already granted.
                        Cursor cur = cr.query(contactData, null, null, null, null);
                        if (cur.getCount() > 0) {// thats mean some resutl has been found
                            if (cur.moveToNext()) {
                                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                                Log.i("Names", name);
                                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                                    // Query phone here. Covered next
                                    Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id, null, null);
                                    while (phones.moveToNext()) {
                                        nmbr = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                        phnnmbr.setText(nmbr, TextView.BufferType.EDITABLE);
                                        Log.i("Number", nmbr);
                                    }
                                    phones.close();
                                }

                            }
                        }

                    }

                }
        }
    }


    // for send message
 /*   public void sendMessage()
    {


         nmbr =phnnmbr.getText().toString();
         mssg = txtmssg.getText().toString();

         if(phnnmbr.equals("") && mssg.equals(""))
         {Toast.makeText(getBaseContext(),"Either Number or Text field is empty..!!",Toast.LENGTH_SHORT).show(); }
         else {

             boolean isInsertData = smsDb.insertData(nmbr, mssg, "pending");

             if (isInsertData) {
                 Toast.makeText(getBaseContext(), "Data Inserted", Toast.LENGTH_SHORT).show();
             } else {
                 Toast.makeText(getBaseContext(), "Data not Inserted", Toast.LENGTH_SHORT).show();
             }
         }


     /*   String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
                new Intent(SENT), 0);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        // When sms sent
        registerReceiver(new BroadcastReceiver(){  //  Sms delivery output
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS sent",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "Generic failure",
                                Toast.LENGTH_LONG).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "No service",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "Null PDU",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getBaseContext(), "Radio off",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));


        //---when the SMS has been delivered---
        registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), "SMS not delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));



        // check permission granted and if dont the permission ask for permission
        if (ContextCompat.checkSelfPermission(this,permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED )
        {
            ActivityCompat.requestPermissions(this, new String[]{permission.SEND_SMS},MY_PERMISSIONS_REQUEST_SEND_SMS);
        }
        else
        {
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(nmbr,null,mssg,sentPI,deliveredPI);
            Toast.makeText(getBaseContext(),"Number: " + nmbr + "\nmessage: " + mssg + "\nSMS Sent..!",Toast.LENGTH_SHORT).show();
        }


    }  */

    //To send SMS
    public void action_setAlarm() {

        setAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setAlarm();
                cencel.setVisibility(View.VISIBLE);
            }
        });

    }

    public boolean setAlarm() {
        AlarmManager aM = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        if (nmbr == null) {
            nmbr = phnnmbr.getText().toString();
        }
        mssg = txtmssg.getText().toString();

        if ("".equals(phnnmbr) || "".equals(mssg)) {
            Toast.makeText(getBaseContext(), "Either Number or Text field is empty..!!", Toast.LENGTH_SHORT).show();
            return false;

        } else {

            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, Year);
            cal.set(Calendar.MONTH, month);
            cal.set(Calendar.DAY_OF_MONTH, day);
            cal.set(Calendar.HOUR_OF_DAY, hour);
            cal.set(Calendar.MINUTE, Minute);
            cal.set(Calendar.SECOND, 0);
            dateTime = String.valueOf(cal.getTime());
            Log.i("Date & Time :", String.valueOf(cal.getTime()));

            boolean isInsertData = smsDb.insertData(nmbr, mssg, "pending", dateTime);

            if (isInsertData) {
                Toast.makeText(getBaseContext(), "Data Inserted", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getBaseContext(), "Data not Inserted", Toast.LENGTH_SHORT).show();
            }


            Intent i = new Intent(this, MyAlarm.class);

            Log.i("Sms", "number :" + nmbr);
            i.putExtra("number", nmbr);
            Log.i("Sms", "txt :" + mssg);
            i.putExtra("DateTime", dateTime);


            //creating a pending intent using the intent
            PendingIntent pi = PendingIntent.getBroadcast(this, FLAG, i, PendingIntent.FLAG_UPDATE_CURRENT);
            FLAG++;

            aM.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pi);

            Toast.makeText(getBaseContext(), "Schedule set..!!", Toast.LENGTH_SHORT).show();
            return true;

        }
    }

    public void cencelAlarm() {
        AlarmManager aM = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, MyAlarm.class);
        PendingIntent pi = PendingIntent.getBroadcast(this, FLAG - 1, intent, PendingIntent.FLAG_UPDATE_CURRENT );
        aM.cancel(pi);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
