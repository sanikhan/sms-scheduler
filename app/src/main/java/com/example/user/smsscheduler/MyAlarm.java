package com.example.user.smsscheduler;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

public class MyAlarm extends BroadcastReceiver {

    SmsManager sms = SmsManager.getDefault(); // smsmanager is used to cobtrol sending sms
    String message,number,dateTime;

    @Override
    public IBinder peekService(Context myContext, Intent service) {
        return super.peekService(myContext, service);
    }

    @Override //recieve broadcast signal in alarm time
    public void onReceive(Context context, Intent intent) {
        SmsDataBase sD = new SmsDataBase(context);

        Bundle extrabundle = intent.getExtras();
        assert extrabundle != null;
             number = extrabundle.getString("number");
             dateTime = extrabundle.getString("DateTime");
        Log.i("Broadcasting value","Number :"+number);
        Log.i("Broadcasting value","Date & Time :"+dateTime);
        Log.i("Alarm","Alarrm is firing");


        Cursor c = sD.getDataByNumber_DateTime(number,dateTime);
        if (c != null)
        {
          c.moveToFirst();
          do {
               message = c.getString(2);
          }while (c.moveToNext());
          Log.i("Broadcast Number:",number);
            Log.i("Broadcast mssg:",message);
            Log.i("Broadcast DateTime:",message);
        }



        // send sms
   //   sms.sendTextMessage(nmbr,null,message,null,null);

        // send number and text to main activity to update database
        Intent i = new Intent("com/example/user/smsscheduler/MainActivity.java");
        i.putExtra("Number",number);
        i.putExtra("DateTime",dateTime);
        context.sendBroadcast(i);
    //end

       // Toast.makeText(context,"Sms Sent....!!!",Toast.LENGTH_SHORT).show();
    }
}
