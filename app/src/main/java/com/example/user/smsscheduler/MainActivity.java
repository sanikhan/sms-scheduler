package com.example.user.smsscheduler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    //Globar variables
    android.support.v7.widget.Toolbar toolbar;
    DrawerLayout drawerLayout;
    Intent intent;
    SmsDataBase smsDataBase;
    ListView listView;
    ArrayList<String>info;
    ArrayAdapter<String> adapter;
    String number = "";
    TextView  nodata_text;
    String dateTime = "";
    /// end global vraiables
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar= findViewById(R.id.toolbar); // for tool bar
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("SMS Scheduler"); // set title in toolbar
        ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.nav_menu); //navigation drawer icon set
        ab.setDisplayHomeAsUpEnabled(true);
        nodata_text = findViewById(R.id.no_data);

        smsDataBase = new SmsDataBase(this);  // initialyzing database
        info = new ArrayList<>();      // get all data of database in info
        listView= findViewById(R.id.lV1);   // initialyzing list view
        adapter= new ArrayAdapter<>(this,R.layout.data_dispaly,R.id.dd_item,info);
        adapter.clear();
        ViewData();    //  view database data in list view
        action_navigation_drawer();


        // Recieve phone number and message that has benn sent from MyAlarm Broadcast reciever by intent
        BroadcastReceiver broadcastReceiver =  new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) { // Registered broadcast reciever in class

                Bundle b = intent.getExtras();

                 number = b.getString("Number");
                 dateTime = b.getString("DateTime");
              //  b.clear();
                int nOr = smsDataBase.update(number,dateTime);
                if(nOr == 0 )
                {
                    Toast.makeText(context,"data not updated",Toast.LENGTH_SHORT).show();
                }
                else
                { Toast.makeText(context,"data updated",Toast.LENGTH_SHORT).show();
                    adapter.clear(); // to clear previous data of adapter // to avoid duplicate value in list
                    ViewData(); }
                    Log.i("MainA", "Number: " + number);
                    Log.i("MainA", "txt: " + dateTime);
            }
        };

        registerReceiver(broadcastReceiver, new IntentFilter("com/example/user/smsscheduler/MainActivity.java")); // registration of broadcast reciever
    }
    //////end broadcast reciever class

// select option or buttons of toolbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Integer id= item.getItemId(); // if help button is pressed
        if(id== R.id.help)
        {

            Toast.makeText(getBaseContext(),"Help",Toast.LENGTH_SHORT).show();
        }
        if(id==R.id.add) // if add button is pressed
        {
           intent = new Intent(this,Sms.class);
           startActivity(intent);
        }
        if(id==R.id.refresh)
        {
            adapter.clear();
            ViewData();
            Toast.makeText(getBaseContext(),"Refresh..!!",Toast.LENGTH_SHORT).show();

        }
        if (id==android.R.id.home)
        {

            drawerLayout.openDrawer(GravityCompat.START);

        }
        return super.onOptionsItemSelected(item);
    }
    //end toolbar selection buttons

    // view all data of database in list view
    private void ViewData()
    {

        Cursor cursor = smsDataBase.getDataByStatus("pending");
        cursor.moveToFirst();
        if(cursor.getCount()==0)
        {

            nodata_text.setText("No data To show");
           // Toast.makeText(getBaseContext(),"No data To show",Toast.LENGTH_SHORT).show();
        }
        else
        {
            nodata_text.setVisibility(View.GONE);
            do {
                info.add("Number:" + cursor.getString(1)+ "\nText:" +cursor.getString(2) + "\nStatus: " + cursor.getString(3)+ "\n"+ cursor.getString(4) );
            }while (cursor.moveToNext());
            listView.setAdapter(adapter);
        }
    }
    ///// end view data

    public void action_navigation_drawer()
    {
        drawerLayout=(DrawerLayout)findViewById(R.id.drawer_layout);
        NavigationView  navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                Integer id = menuItem.getItemId();

                if ( id == R.id.Sent)
                {
                    Toast.makeText(getBaseContext(),"Sent",Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });

    }


}